// +build private

package counters

import (
	"testing"

	"gitlab.com/slon/shad-ds/counters/lib"
	"gitlab.com/slon/shad-ds/counters/solution"
	"gitlab.com/slon/shad-ds/private/counters/tests"
)

func TestPrivate(t *testing.T) {
	lib.RunTests(t, solution.NewCountersService, tests.PrivateTests)
}
