package algo

import "gitlab.com/slon/shad-ds/atomic_broadcast/lib"
import (
	"bytes"
	"encoding/binary"
	"fmt"
	"math"
)

const RpcTimeout = lib.AvgDeliveryTime * 3
const PingPeriod = lib.AvgDeliveryTime * 10
const LeaderLeaseTimeout = 3 * PingPeriod
const MaxVotingTimeout = lib.AvgDeliveryTime * 5

const MaxRetryIndex = 15

type PersistentState struct {
	CurrentTerm      int64
	CommittedIndex   int64
	TotalEntrieCount int64
}

type PersistentLogEntry struct {
	MessageId   int64
	Term        int64
	IndexInTerm int64
	Dummy       bool
}

type RaftState int

const (
	Candidate RaftState = 0
	Leader    RaftState = 1
	Follower  RaftState = 2
)

// State of the follower on leader.
type FollowerInfo struct {
	outstandingRequest *ReqAppendEntries
	persistedIndex     int
	retryIndex         int
}

type LeaderState struct {
	followers   map[int]*FollowerInfo
	indexInTerm int
}

type ExampleBroadcaster struct {
	context lib.Context
	term    int

	committedIndex int
	deliveredIndex int

	deliver lib.Deliver

	votedFor int
	state    RaftState

	// Totally ordered log on the node.
	logEntries []PersistentLogEntry

	nodeId    int
	nodeCount int

	leaderState   *LeaderState
	electionState *ElectionState

	// Counter used to generate unique request id.
	requestIndex int
}

//////////////////////////////////////////////////////////////////////////////////

// Used to redirect broadcast request from follower to leader.
type ReqBroadcast struct {
	MessageId int
}

func (broadcaster *ExampleBroadcaster) Broadcast(messageId int) {
	switch broadcaster.state {
	case Follower:
		broadcaster.context.SendMessage(ReqBroadcast{messageId}, broadcaster.votedFor)

	case Leader:
		broadcaster.DoBroadcast(&messageId)
	}
}

func (broadcaster *ExampleBroadcaster) OnMessage(message interface{}, source int) {
	switch x := message.(type) {
	case ReqBroadcast:
		broadcaster.Broadcast(x.MessageId)

	case ReqAppendEntries:
		broadcaster.OnReqAppendEntries(&x, source)

	case RspAppendEntries:
		broadcaster.OnRspAppendEntries(&x, source)

	case ReqGetVote:
		broadcaster.OnReqGetVote(&x, source)

	case RspGetVote:
		broadcaster.OnRspGetVote(&x, source)

	default:
		panic("Unknown message type")
	}
}

//////////////////////////////////////////////////////////////////////////////////

const MaxRandomReqId = 1000000000

func (broadcaster *ExampleBroadcaster) CreateRequestId() string {
	broadcaster.requestIndex++
	return fmt.Sprintf("%v-%v-%v-%v",
		broadcaster.nodeId,
		broadcaster.term,
		broadcaster.requestIndex,
		broadcaster.context.RandomInt(MaxRandomReqId))
}

func (broadcaster *ExampleBroadcaster) ValidateFollowerTerm(term int, source int) bool {
	if broadcaster.term == term && broadcaster.state == Follower {
		return true
	}

	if broadcaster.term < term {
		broadcaster.StartFollowing(source, term)
	}

	return false
}

func (broadcaster *ExampleBroadcaster) ValidateLeaderTerm(term int) bool {
	if broadcaster.term == term && broadcaster.state == Leader {
		return true
	}

	if broadcaster.term < term {
		broadcaster.StartElections(term)
	}

	return false
}

func (broadcaster *ExampleBroadcaster) ValidateLastEntry(lastEntryTerm int, lastEntryIndex int) bool {
	if len(broadcaster.logEntries) == 0 {
		return true
	}

	entry := &broadcaster.logEntries[len(broadcaster.logEntries)-1]

	switch {
	case int(entry.Term) < lastEntryTerm:
		return true

	case int(entry.Term) == lastEntryTerm && int(entry.IndexInTerm) <= lastEntryIndex:
		return true
	}
	return false
}

func (broadcaster *ExampleBroadcaster) ValidatePrevEntry(prevEntryIndex int, prevEntryTerm int, prevEntryIndexInTerm int) bool {
	if prevEntryIndex < 0 {
		return true
	}

	if len(broadcaster.logEntries) <= prevEntryIndex {
		return false
	}

	entry := &broadcaster.logEntries[prevEntryIndex]
	return int(entry.Term) == prevEntryTerm && int(entry.IndexInTerm) == prevEntryIndexInTerm
}

func (broadcaster *ExampleBroadcaster) StartFollowing(leader int, term int) {
	broadcaster.state = Follower
	broadcaster.term = term
	broadcaster.votedFor = leader
	broadcaster.ScheduleLeaderLeaseTimeout()
}

func (broadcaster *ExampleBroadcaster) StartLeading() {
	leaderState := LeaderState{
		followers:   make(map[int]*FollowerInfo, 0),
		indexInTerm: 0}

	broadcaster.state = Leader
	broadcaster.leaderState = &leaderState

	for nodeId := 0; nodeId < broadcaster.nodeCount; nodeId++ {
		if nodeId != broadcaster.nodeId {
			broadcaster.leaderState.followers[nodeId] = &FollowerInfo{
				persistedIndex:     len(broadcaster.logEntries) - 1,
				retryIndex:         0,
				outstandingRequest: nil}
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////

type ReqAppendEntries struct {
	ReqId           string
	Term            int
	PrevTerm        int
	PrevIndexInTerm int
	PrevIndex       int
	Entries         []PersistentLogEntry
	CommittedIndex  int
}

type RspAppendEntries struct {
	ReqId     string
	Term      int
	Success   bool
	PrevIndex int
}

func (broadcaster *ExampleBroadcaster) MakeAppendEntriesReq(fromIndex int) ReqAppendEntries {
	req := ReqAppendEntries{
		ReqId:          broadcaster.CreateRequestId(),
		Term:           broadcaster.term,
		Entries:        append([]PersistentLogEntry{}, broadcaster.logEntries[fromIndex:]...),
		CommittedIndex: broadcaster.committedIndex,
		PrevIndex:      fromIndex - 1}

	if fromIndex > 0 {
		req.PrevTerm = int(broadcaster.logEntries[fromIndex-1].Term)
		req.PrevIndexInTerm = int(broadcaster.logEntries[fromIndex-1].IndexInTerm)
	} else {
		req.PrevTerm = -1
		req.PrevIndexInTerm = -1
	}

	return req
}

func (broadcaster *ExampleBroadcaster) SendAppendEntriesToFollowers() {
	for nodeId, follower := range broadcaster.leaderState.followers {
		if follower.outstandingRequest == nil {
			broadcaster.SendAppendEntries(nodeId, follower)
		}
	}
}

func (broadcaster *ExampleBroadcaster) SendAppendEntries(nodeId int, follower *FollowerInfo) {
	req := broadcaster.MakeAppendEntriesReq(follower.persistedIndex + 1)
	follower.outstandingRequest = &req
	broadcaster.context.SendMessage(req, nodeId)

	broadcaster.context.AfterFunc(RpcTimeout, func() {
		broadcaster.OnAppendEntriesTimeout(nodeId, &req)
	})
}

func (broadcaster *ExampleBroadcaster) OnReqAppendEntries(req *ReqAppendEntries, source int) {
	rsp := RspAppendEntries{ReqId: req.ReqId, Success: false}

	if broadcaster.ValidateFollowerTerm(req.Term, source) {
		if broadcaster.ValidatePrevEntry(req.PrevIndex, req.PrevTerm, req.PrevIndexInTerm) {
			broadcaster.logEntries = append(broadcaster.logEntries[:req.PrevIndex+1], req.Entries...)
			broadcaster.PersistFromIndex(req.PrevIndex + 1)
			rsp.Success = true
			broadcaster.DoAdvanceCommittedIndex(req.CommittedIndex)
			broadcaster.ScheduleLeaderLeaseTimeout()
		}
	}
	rsp.PrevIndex = len(broadcaster.logEntries)
	rsp.Term = broadcaster.term
	broadcaster.context.SendMessage(rsp, source)
}

func (broadcaster *ExampleBroadcaster) OnRspAppendEntries(rsp *RspAppendEntries, source int) {
	if !broadcaster.ValidateLeaderTerm(rsp.Term) {
		return
	}

	follower := broadcaster.leaderState.followers[source]
	if follower.outstandingRequest == nil || follower.outstandingRequest.ReqId != rsp.ReqId {
		return
	}

	req := follower.outstandingRequest
	follower.outstandingRequest = nil
	if rsp.Success {
		follower.retryIndex = 0
		follower.persistedIndex = req.PrevIndex + len(req.Entries)
		broadcaster.AdvanceLeaderCommittedIndex()

		if len(broadcaster.logEntries) > follower.persistedIndex+1 {
			broadcaster.SendAppendEntries(source, follower)
		}
	} else {
		// Adjust persisted index and retry sending.
		if follower.persistedIndex > rsp.PrevIndex {
			follower.persistedIndex = rsp.PrevIndex
		}
		if follower.retryIndex < MaxRetryIndex {
			follower.retryIndex++
		}
		follower.persistedIndex -= int(math.Pow(2, float64(follower.retryIndex)))
		if follower.persistedIndex < 0 {
			follower.persistedIndex = -1
		}
		broadcaster.SendAppendEntries(source, follower)
	}
}

func (broadcaster *ExampleBroadcaster) OnAppendEntriesTimeout(nodeId int, req *ReqAppendEntries) {
	if !broadcaster.ValidateLeaderTerm(req.Term) {
		return
	}

	follower := broadcaster.leaderState.followers[nodeId]
	if follower.outstandingRequest == nil || follower.outstandingRequest.ReqId != req.ReqId {
		return
	}

	follower.outstandingRequest = nil
	broadcaster.SendAppendEntries(nodeId, follower)
}

//////////////////////////////////////////////////////////////////////////////////

type ElectionState struct {
	req          *ReqGetVote
	successCount int
	votes        map[int]bool
}

type ReqGetVote struct {
	ReqId                string
	Term                 int
	LastEntryTerm        int
	LastEntryIndexInTerm int
}

type RspGetVote struct {
	ReqId       string
	Term        int
	VoteGranted bool
}

func (broadcaster *ExampleBroadcaster) MakeGetVoteRequest() ReqGetVote {
	req := ReqGetVote{
		ReqId: broadcaster.CreateRequestId(),
		Term:  broadcaster.term}

	if len(broadcaster.logEntries) > 0 {
		lastEntry := &broadcaster.logEntries[len(broadcaster.logEntries)-1]
		req.LastEntryTerm = int(lastEntry.Term)
		req.LastEntryIndexInTerm = int(lastEntry.IndexInTerm)
	} else {
		req.LastEntryTerm = -1
		req.LastEntryIndexInTerm = -1
	}

	return req
}

func (broadcaster *ExampleBroadcaster) StartElections(term int) {
	broadcaster.term = term
	broadcaster.state = Candidate
	broadcaster.electionState = nil
	broadcaster.leaderState = nil

	broadcaster.context.AfterFunc(lib.MinTimerDelay+broadcaster.context.RandomInt(MaxVotingTimeout), func() {
		if broadcaster.state != Candidate || broadcaster.electionState != nil {
			return
		}

		broadcaster.term++
		req := broadcaster.MakeGetVoteRequest()
		broadcaster.electionState = &ElectionState{
			req:          &req,
			successCount: 1,
			votes:        make(map[int]bool)}

		broadcaster.PersistMeta()
		for nodeId := 0; nodeId < broadcaster.nodeCount; nodeId++ {
			if nodeId != broadcaster.nodeId {
				broadcaster.electionState.votes[nodeId] = false
				broadcaster.context.SendMessage(req, nodeId)
			}
		}

		broadcaster.context.AfterFunc(lib.MinTimerDelay+broadcaster.context.RandomInt(MaxVotingTimeout), func() {
			broadcaster.OnVotingTimeout(&req)
		})
	})
}

func (broadcaster *ExampleBroadcaster) OnReqGetVote(req *ReqGetVote, source int) {
	rsp := RspGetVote{ReqId: req.ReqId, VoteGranted: false}

	switch {
	case broadcaster.term == req.Term && broadcaster.votedFor == source:
		rsp.VoteGranted = true

	case broadcaster.term < req.Term:
		if broadcaster.ValidateLastEntry(req.LastEntryTerm, req.LastEntryIndexInTerm) {
			broadcaster.StartFollowing(source, req.Term)
			rsp.VoteGranted = true
		} else {
			broadcaster.StartElections(req.Term)
		}
	}

	rsp.Term = broadcaster.term
	broadcaster.context.SendMessage(rsp, source)
}

func (broadcaster *ExampleBroadcaster) OnRspGetVote(rsp *RspGetVote, source int) {
	if broadcaster.state != Candidate || broadcaster.electionState == nil || broadcaster.electionState.req.ReqId != rsp.ReqId {
		return
	}

	if broadcaster.term < rsp.Term || !rsp.VoteGranted {
		broadcaster.StartElections(rsp.Term)
		return
	}

	if !broadcaster.electionState.votes[source] {
		broadcaster.electionState.votes[source] = true
		broadcaster.electionState.successCount++
		if broadcaster.electionState.successCount > broadcaster.nodeCount/2 {
			broadcaster.StartLeading()
		}
	}
}

func (broadcaster *ExampleBroadcaster) OnVotingTimeout(req *ReqGetVote) {
	if broadcaster.state != Candidate || broadcaster.electionState == nil || broadcaster.electionState.req.ReqId != req.ReqId {
		return
	}

	for nodeId := 0; nodeId < broadcaster.nodeCount; nodeId++ {
		if nodeId != broadcaster.nodeId && !broadcaster.electionState.votes[nodeId] {
			broadcaster.context.SendMessage(*req, nodeId)
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////

const PersistentStateSize = 24

func (broadcaster *ExampleBroadcaster) PersistMeta() {
	var buffer bytes.Buffer

	ps := PersistentState{
		CurrentTerm:      int64(broadcaster.term),
		CommittedIndex:   int64(broadcaster.committedIndex),
		TotalEntrieCount: int64(len(broadcaster.logEntries))}

	binary.Write(&buffer, binary.LittleEndian, ps)
	// Errors are possible only if we die.
	broadcaster.context.WriteAt(buffer.Bytes(), 0)
}

func (broadcaster *ExampleBroadcaster) PersistFromIndex(index int) {
	broadcaster.PersistMeta()

	var buffer bytes.Buffer

	binary.Write(&buffer, binary.LittleEndian, broadcaster.logEntries[index:])
	offset := PersistentStateSize
	if index > 0 {
		offset += binary.Size(broadcaster.logEntries[:index])
	}

	broadcaster.context.WriteAt(buffer.Bytes(), int64(offset))
}

func (broadcaster *ExampleBroadcaster) ScheduleLeaderLeaseTimeout() {
	logPrefixLength := len(broadcaster.logEntries)
	term := broadcaster.term

	broadcaster.context.AfterFunc(LeaderLeaseTimeout, func() {
		if broadcaster.state == Follower && broadcaster.term == term && len(broadcaster.logEntries) == logPrefixLength {
			broadcaster.StartElections(term)
		}
	})
}

func (broadcaster *ExampleBroadcaster) ScheduleBroadcastDummyMessage() {
	logPrefixLength := len(broadcaster.logEntries)
	broadcaster.context.AfterFunc(PingPeriod, func() {
		if broadcaster.state == Leader && len(broadcaster.logEntries) == logPrefixLength {
			broadcaster.DoBroadcast(nil)
		}
	})
}

func (broadcaster *ExampleBroadcaster) DoBroadcast(messageId *int) {
	broadcaster.leaderState.indexInTerm++
	var entry PersistentLogEntry
	if messageId == nil {
		entry = PersistentLogEntry{
			MessageId:   -1,
			Dummy:       true,
			Term:        int64(broadcaster.term),
			IndexInTerm: int64(broadcaster.leaderState.indexInTerm)}
	} else {
		entry = PersistentLogEntry{
			MessageId:   int64(*messageId),
			Dummy:       false,
			Term:        int64(broadcaster.term),
			IndexInTerm: int64(broadcaster.leaderState.indexInTerm)}
	}

	broadcaster.logEntries = append(broadcaster.logEntries, entry)
	broadcaster.PersistFromIndex(len(broadcaster.logEntries) - 1)
	broadcaster.SendAppendEntriesToFollowers()
	broadcaster.ScheduleBroadcastDummyMessage()
}

func (broadcaster *ExampleBroadcaster) AdvanceLeaderCommittedIndex() {
	for {
		committedCount := 1
		for _, follower := range broadcaster.leaderState.followers {
			if broadcaster.committedIndex+1 <= follower.persistedIndex {
				committedCount++
			}
		}

		if committedCount <= broadcaster.nodeCount/2 {
			return
		}

		broadcaster.DoAdvanceCommittedIndex(broadcaster.committedIndex + 1)
	}
}

func (broadcaster *ExampleBroadcaster) DoAdvanceCommittedIndex(committedIndex int) {
	for i := broadcaster.deliveredIndex + 1; i <= committedIndex; i++ {
		if !broadcaster.logEntries[i].Dummy {
			broadcaster.deliver(int(broadcaster.logEntries[i].MessageId))
		}
	}
	if committedIndex > broadcaster.committedIndex {
		broadcaster.deliveredIndex = committedIndex
		broadcaster.committedIndex = committedIndex
	}
}

//////////////////////////////////////////////////////////////////////////////////

func NewExample(nodeId, nodeCount int, deliver lib.Deliver, context lib.Context) lib.AtomicBroadcaster {
	if binary.Size(&PersistentState{}) != PersistentStateSize {
		err := fmt.Errorf("Invalid size of persistent state (%v)", binary.Size(PersistentState{}))
		panic(err)
	}

	broadcaster := ExampleBroadcaster{
		context:        context,
		deliver:        deliver,
		nodeId:         nodeId,
		nodeCount:      nodeCount,
		term:           0,
		committedIndex: -1,
		deliveredIndex: -1,
		requestIndex:   0,
		logEntries:     make([]PersistentLogEntry, 0)}

	if context.GetStorageLength() > 0 {
		var ps PersistentState
		data := make([]byte, PersistentStateSize)
		_, err := context.ReadAt(data, 0)
		if err != nil {
			return nil
		}
		reader := bytes.NewReader(data)
		binary.Read(reader, binary.LittleEndian, &ps)

		broadcaster.term = int(ps.CurrentTerm)
		broadcaster.committedIndex = int(ps.CommittedIndex)

		data = make([]byte, context.GetStorageLength()-PersistentStateSize)
		n, err := context.ReadAt(data, PersistentStateSize)
		if err != nil {
			broadcaster.committedIndex = -1
		} else {
			if n != len(data) {
				err := fmt.Errorf("Failed to read enough from persistent storage (NodeId: %v, Expected: %v, Actual: %v)",
					broadcaster.nodeId,
					len(data),
					n)
				panic(err)
			}

			reader = bytes.NewReader(data)
			broadcaster.logEntries = make([]PersistentLogEntry, ps.TotalEntrieCount)
			binary.Read(reader, binary.LittleEndian, broadcaster.logEntries)

			if len(broadcaster.logEntries) <= broadcaster.committedIndex {
				err = fmt.Errorf("In revival less log entries than committed (NodeId: %v, CommitedIndex: %v, EntrieCount: %v, TotalEntrieCount: %v)",
					broadcaster.nodeId,
					broadcaster.committedIndex,
					len(broadcaster.logEntries),
					ps.TotalEntrieCount)
				panic(err)
			}

			for index := 0; index <= broadcaster.committedIndex; index++ {
				if !broadcaster.logEntries[index].Dummy {
					deliver(int(broadcaster.logEntries[index].MessageId))
				}
			}

			broadcaster.deliveredIndex = broadcaster.committedIndex
		}
	}

	broadcaster.StartElections(broadcaster.term)
	return &broadcaster
}
