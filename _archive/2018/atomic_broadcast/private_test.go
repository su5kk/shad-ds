// +build private

package atomic_broadcast

import (
	"gitlab.com/slon/shad-ds/atomic_broadcast/algo"
	"gitlab.com/slon/shad-ds/atomic_broadcast/lib"
	tests2 "gitlab.com/slon/shad-ds/private/atomic_broadcast/tests"
	"testing"
)

func TestSolutionIsPassingPrivateTests(t *testing.T) {
	if algo.HasSolution {
		lib.RunTests(t, algo.NewSolution, tests2.Tests, false)
	}
}
