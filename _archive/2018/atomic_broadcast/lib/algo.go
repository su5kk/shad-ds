package lib

type TimerCallback func()

// Provides utilities to be used by atomic broadcaster.
type Context interface {
	// Sends message to any other node.
	SendMessage(message interface{}, destination int)

	// Timer function.
	AfterFunc(duration int, f TimerCallback)

	// Persistent storage.

	// Writes len(p) bytes from p starting from given offset.
	// Returns number of written bytes and an error, if happened.
	WriteAt(p []byte, off int64) (n int, err error)

	// Reads up to len(p) bytes from storage into p, starting from given offset.
	// Returns number of read bytes and an error, if happened.
	ReadAt(p []byte, off int64) (n int, err error)

	// Current amount of written bytes.
	GetStorageLength() int

	RandomInt(n int) int
}

type AtomicBroadcaster interface {
	// Broadcasts message to all peers.
	Broadcast(messageId int)

	// OnMessage is called by context, when a message is delivered.
	OnMessage(message interface{}, source int)
}

// Is called by the broadcaster, when the message is reliably stored and ordered.
type Deliver func(messageId int)

type AtomicBroadcasterCtor func(nodeId, nodeCount int, deliver Deliver, context Context) AtomicBroadcaster
