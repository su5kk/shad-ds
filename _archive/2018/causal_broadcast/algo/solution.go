package algo

import "gitlab.com/slon/shad-ds/causal_broadcast/lib"

var HasSolution bool = false

func NewSolution(nodeId, nodeCount int, deliver lib.Deliver, transport lib.UnderlyingTransport) lib.CausalBroadcaster {
	panic("Implement me")
}
