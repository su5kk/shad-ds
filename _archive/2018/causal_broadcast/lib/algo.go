package lib

// This is FifoPerfectLink.
type UnderlyingTransport interface {
	SendMessage(message interface{}, destination int)
}

type CausalBroadcaster interface {
	// Broadcasts message to all alive peers.
	Broadcast(messageId int)

	// OnMessage is called by underlying transport, when a message is delivered.
	OnMessage(message interface{}, source int)

	// OnNodeDeath is called by underlying transport, when remote node dies.
	// No messages will be deleivered from this source after OnNodeDeath is called.
	OnNodeDeath(source int)
}

type Deliver func(messageId int)

type CausalBroadcasterCtor func(nodeId, nodeCount int, deliver Deliver, transport UnderlyingTransport) CausalBroadcaster
