package lib

import (
	"container/heap"
	"fmt"
	"runtime/debug"
	"testing"
	"time"
)

type Vertex struct {
	MessageId           int
	ShouldSend          bool
	Broadcasted         bool
	DependentMessageIds []int
	MetDependecyCount   int
	TotalDependecyCount int
	SendRequest         *SendRequest
}

func BuildDependecyGraph(sendRequests []SendRequest) map[int]*Vertex {
	result := map[int]*Vertex{}

	for i, sendRequest := range sendRequests {
		vertex, ok := result[sendRequest.MessageId]
		if ok {
			vertex.TotalDependecyCount = len(sendRequest.HappenedBeforeMessages)
			vertex.SendRequest = &sendRequests[i]
		} else {
			result[sendRequest.MessageId] = &Vertex{
				SendRequest:         &sendRequests[i],
				MessageId:           sendRequest.MessageId,
				ShouldSend:          true,
				DependentMessageIds: []int{},
				TotalDependecyCount: len(sendRequest.HappenedBeforeMessages)}
		}

		for _, hbMessageId := range sendRequest.HappenedBeforeMessages {
			vertex, ok := result[hbMessageId]
			if ok {
				vertex.DependentMessageIds = append(vertex.DependentMessageIds, sendRequest.MessageId)
			} else {
				result[hbMessageId] = &Vertex{
					ShouldSend:          true,
					MessageId:           hbMessageId,
					DependentMessageIds: []int{sendRequest.MessageId}}
			}
		}
	}
	return result
}

func ClearDependencyGraph(graph map[int]*Vertex) {
	for _, vertex := range graph {
		vertex.MetDependecyCount = 0
	}
}

//////////////////////////////////////////////////////////////////////////////////

type TestNode struct {
	ReceivedIds      []int
	DependencyGraph  map[int]*Vertex
	ReadyToBroadcast []int
	Failed           bool
	Dead             bool
}

func DoDeliver(testNode *TestNode, messageId int) {
	if testNode.Failed {
		return
	}

	testNode.ReceivedIds = append(testNode.ReceivedIds, messageId)
	receivedVertex, ok := testNode.DependencyGraph[messageId]
	if !ok {
		return
	}

	for _, id := range receivedVertex.DependentMessageIds {
		vertex := testNode.DependencyGraph[id]
		vertex.MetDependecyCount++
		if vertex.MetDependecyCount == vertex.TotalDependecyCount && vertex.ShouldSend && !vertex.Broadcasted {
			if vertex.MessageId < 0 {
				testNode.Failed = true
			} else {
				testNode.ReadyToBroadcast = append(testNode.ReadyToBroadcast, vertex.MessageId)
			}

			vertex.Broadcasted = true
		}
	}
}

func (testNode *TestNode) Init(sendRequests []SendRequest, nodeId int) {
	testNode.Failed = false
	testNode.DependencyGraph = make(map[int]*Vertex)
	testNode.ReceivedIds = []int{}
	testNode.ReadyToBroadcast = []int{}

	for _, sendRequest := range sendRequests {
		if sendRequest.Source != nodeId {
			continue
		}

		if len(sendRequest.HappenedBeforeMessages) == 0 {
			if sendRequest.MessageId < 0 {
				testNode.Failed = true
			} else {
				testNode.ReadyToBroadcast = append(testNode.ReadyToBroadcast, sendRequest.MessageId)
			}
			continue
		}

		vertex, ok := testNode.DependencyGraph[sendRequest.MessageId]
		if ok {
			vertex.ShouldSend = true
			vertex.TotalDependecyCount = len(sendRequest.HappenedBeforeMessages)
		} else {
			testNode.DependencyGraph[sendRequest.MessageId] = &Vertex{
				MessageId:           sendRequest.MessageId,
				ShouldSend:          true,
				DependentMessageIds: []int{},
				TotalDependecyCount: len(sendRequest.HappenedBeforeMessages)}
		}

		for _, hbMessageId := range sendRequest.HappenedBeforeMessages {
			vertex, ok := testNode.DependencyGraph[hbMessageId]
			if ok {
				vertex.DependentMessageIds = append(vertex.DependentMessageIds, sendRequest.MessageId)
			} else {
				testNode.DependencyGraph[hbMessageId] = &Vertex{
					MessageId:           hbMessageId,
					DependentMessageIds: []int{sendRequest.MessageId}}
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////

type DeliveryQueue []SentMessage

func (q DeliveryQueue) Len() int { return len(q) }

func (q DeliveryQueue) Less(i, j int) bool {
	return q[i].DeliverTimestamp < q[j].DeliverTimestamp
}

func (q DeliveryQueue) Swap(i, j int) {
	q[i], q[j] = q[j], q[i]
}

func (q *DeliveryQueue) Push(x interface{}) {
	sentMessage := x.(SentMessage)
	*q = append(*q, sentMessage)
}

func (q *DeliveryQueue) Pop() interface{} {
	old := *q
	n := len(old)
	item := old[n-1]
	*q = old[0 : n-1]
	return item
}

//////////////////////////////////////////////////////////////////////////////////

type TestTransport struct {
	source                    int
	currentDeliveryTimestamps []int
	delayFunc                 func(message SentMessage) int
	deliveryQueue             *DeliveryQueue
	timestamp                 *int
	failed                    *bool
	blockedDirections         map[int]bool
}

func (transport *TestTransport) SendMessage(message interface{}, destination int) {
	transport.DoSendMessage(message, destination, false)
}

func (transport *TestTransport) DoSendMessage(message interface{}, destination int, tombstone bool) {
	if !tombstone && *transport.failed {
		_, ok := transport.blockedDirections[destination]
		if ok {
			return
		}
	}

	if transport.currentDeliveryTimestamps[destination] < *transport.timestamp {
		transport.currentDeliveryTimestamps[destination] = *transport.timestamp
	}

	transport.currentDeliveryTimestamps[destination]++

	sentMessage := SentMessage{
		Tombstone:        tombstone,
		Destination:      destination,
		Message:          message,
		Source:           transport.source,
		SendTimestamp:    *transport.timestamp,
		DeliverTimestamp: transport.currentDeliveryTimestamps[destination]}

	deliverTimestamp := transport.delayFunc(sentMessage)

	if deliverTimestamp < transport.currentDeliveryTimestamps[destination] {
		panic("Invalid DelayFunc in test, deliver timestamp is in the past")
	}

	sentMessage.DeliverTimestamp = deliverTimestamp

	transport.currentDeliveryTimestamps[destination] = deliverTimestamp

	heap.Push(transport.deliveryQueue, sentMessage)
}

//////////////////////////////////////////////////////////////////////////////////

func ValidateUniqueMessageIds(sendRequests []SendRequest) error {
	uniqueIds := map[int]bool{}
	for _, sendRequest := range sendRequests {
		_, ok := uniqueIds[sendRequest.MessageId]
		if ok {
			return fmt.Errorf("Duplicate message ids in send requests (MessageId: %v)", sendRequest.MessageId)
		}

		uniqueIds[sendRequest.MessageId] = true
	}
	return nil
}

func ValidateUniqueFailureSources(failureDescriptors []FailureDescriptor) error {
	uniqueIds := map[int]bool{}
	for _, failureDescriptor := range failureDescriptors {
		_, ok := uniqueIds[failureDescriptor.Source]
		if ok {
			return fmt.Errorf("Multiple failure descriptors for the same source (Source: %v)", failureDescriptor.Source)
		}

		uniqueIds[failureDescriptor.Source] = true
	}
	return nil
}

func RunTest(test TestDescriptor, nodeCtor CausalBroadcasterCtor, validateAllBroadcasted bool) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("%v\nStack:\n%s\n", e, debug.Stack())
		}
	}()

	if error := ValidateUniqueMessageIds(test.SendRequests); error != nil {
		return error
	}

	if error := ValidateUniqueFailureSources(test.FailureDescriptors); error != nil {
		return error
	}

	timestamp := 0

	broadcasters := make([]CausalBroadcaster, test.NodeCount)
	testNodes := make([]*TestNode, test.NodeCount)
	transports := make([]*TestTransport, test.NodeCount)

	deliveryQueue := make(DeliveryQueue, 0, 10)
	heap.Init(&deliveryQueue)

	failureRequests := []SendRequest{}
	blockedDirections := make([]map[int]bool, test.NodeCount)

	for _, failureDescriptor := range test.FailureDescriptors {
		failureRequests = append(failureRequests, SendRequest{
			failureDescriptor.HappenedBeforeMessages,
			-1,
			failureDescriptor.Source})

		blockedDirections[failureDescriptor.Source] = map[int]bool{}
		for _, destination := range failureDescriptor.BlockedDirections {
			blockedDirections[failureDescriptor.Source][destination] = true
		}
	}

	sendRequests := append([]SendRequest{}, test.SendRequests...)
	sendRequests = append(sendRequests, failureRequests...)

	// Create broadcasters.
	for i := 0; i < test.NodeCount; i++ {
		testNode := &TestNode{}
		testNode.Init(sendRequests, i)
		testNodes[i] = testNode

		deliver := func(messageId int) {
			DoDeliver(testNode, messageId)
		}

		transport := TestTransport{
			source: i,
			currentDeliveryTimestamps: make([]int, test.NodeCount),
			deliveryQueue:             &deliveryQueue,
			delayFunc:                 test.DelayFunc,
			failed:                    &testNodes[i].Failed,
			timestamp:                 &timestamp,
			blockedDirections:         blockedDirections[i]}
		transports[i] = &transport
		broadcasters[i] = nodeCtor(i, test.NodeCount, deliver, &transport)
	}

	shouldContinue := true
	for shouldContinue {
		shouldContinue = false

		for i, testNode := range testNodes {
			if testNode.Dead {
				continue
			}

			for _, messageId := range testNode.ReadyToBroadcast {
				broadcasters[i].Broadcast(messageId)
				shouldContinue = true
			}
			testNode.ReadyToBroadcast = testNode.ReadyToBroadcast[:0]

			if testNode.Failed {
				for j := 0; j < test.NodeCount; j++ {
					if j != i {
						transports[i].DoSendMessage(nil, j, true)
					}
				}
				testNode.Dead = true
				shouldContinue = true
			}
		}

		if len(deliveryQueue) > 0 {
			sentMessage := heap.Pop(&deliveryQueue).(SentMessage)
			if !testNodes[sentMessage.Destination].Failed {
				if sentMessage.Tombstone {
					broadcasters[sentMessage.Destination].OnNodeDeath(sentMessage.Source)
				} else {
					broadcasters[sentMessage.Destination].OnMessage(sentMessage.Message, sentMessage.Source)
				}
			}

			timestamp = sentMessage.DeliverTimestamp
			shouldContinue = true
		}

		if timestamp > 8 * MaxTimestamp {
			return fmt.Errorf("Simulation exceeded max timestamp")
		}
	}

	// Validate everything was broadcasted.
	if validateAllBroadcasted {
		for i, testNode := range testNodes {
			for _, vertex := range testNode.DependencyGraph {
				if vertex.ShouldSend && !vertex.Broadcasted {
					return fmt.Errorf("One of the messages was not broadcasted (MessageId: %v, Source: %v)",
						vertex.MessageId,
						i)
				}
			}
		}
	}

	// Validate every failed node is dead.
	for i, testNode := range testNodes {
		if testNode.Failed && !testNode.Dead {
			return fmt.Errorf("Internal test error: node has failed but is not dead (Node: %v)", i)
		}
	}

	// Validate every node delivered messages in causal order.
	graph := BuildDependecyGraph(test.SendRequests)
	for i, testNode := range testNodes {
		ClearDependencyGraph(graph)
		for _, messageId := range testNode.ReceivedIds {
			vertex, ok := graph[messageId]
			if !ok {
				return fmt.Errorf("Received unknown message (Node: %v, MessageId: %v)", i, messageId)
			}

			if vertex.TotalDependecyCount != vertex.MetDependecyCount {
				return fmt.Errorf("Causal order violation (Node: %v, MessageId: %v, ShouldHappenedBefore: %v)",
					i,
					messageId,
					vertex.SendRequest.HappenedBeforeMessages)
			}

			for _, dependentMessageId := range vertex.DependentMessageIds {
				graph[dependentMessageId].MetDependecyCount++
			}
		}
	}

	// Validate every non-failed node delivered the same set of messages.
	healthyNodes := []*TestNode{}
	for _, node := range testNodes {
		if !node.Dead {
			healthyNodes = append(healthyNodes, node)
		}
	}

	if len(healthyNodes) > 1 {
		validatationIds := map[int]bool{}
		for _, messageId := range healthyNodes[0].ReceivedIds {
			validatationIds[messageId] = true
		}

		for i := 1; i < len(healthyNodes); i++ {
			for _, messageId := range healthyNodes[i].ReceivedIds {
				_, ok := validatationIds[messageId]
				if !ok {
					return fmt.Errorf("Healthy nodes received different set of messages (MessageId: %v)", messageId)
				}

				validatationIds[messageId] = false
			}

			for messageId, value := range validatationIds {
				if value {
					return fmt.Errorf("Healthy nodes received different set of messages (MessageId: %v)", messageId)
				}
				validatationIds[messageId] = true
			}
		}
	}

	return nil
}

func RunTests(t *testing.T, algo CausalBroadcasterCtor, tests []TestDescriptor, shouldFail bool, validateAllBroadcasted bool) {
	var failed bool

	startTime := time.Now()

	for _, test := range tests {
		if time.Since(startTime) > TestSuitTimeout {
			t.Errorf("Test suit timed out")
		}

		fmt.Printf("Running test %v by %v\n", test.TestName, test.Author)

		t.Run(fmt.Sprintf("%q by %s", test.TestName, test.Author), func(t *testing.T) {
			resultChannel := make(chan error)
			go func() {
				resultChannel <- RunTest(test, algo, validateAllBroadcasted)
			}()

			select {
			case err := <-resultChannel:
				if err != nil {
					failed = true
				}

				if err != nil && !shouldFail {
					t.Errorf("Test failed: %v", err)
				}

			case <-time.After(TestTimeout):
				t.Errorf("Test timed out")
			}
		})
	}

	if shouldFail && !failed {
		t.Errorf("None of the tests failed")
	}
}
