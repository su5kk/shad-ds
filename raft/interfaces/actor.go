package interfaces

import (
	"fmt"
	"go.uber.org/zap"
	"google.golang.org/protobuf/proto"
)

const MaxMessageSize = 100 * 1024

// Average time of one-way remote message delivery in stable segment.
const AvgDeliveryTime = 1000

// Minimum value of delay used in AfterFunc.
const MinTimerDelay = 10

type Message struct {
	Payload proto.Message
	Sender  string
}

func GetActorId(nodeIndex int) string {
	return fmt.Sprintf("actor-%v", nodeIndex)
}

type TimerCallback func()

type Actor interface {
	// Broadcasts message to all peers.
	AtomicBroadcast(broadcastValue int64)

	Receive(message Message)
}

// Provides utilities to be used by atomic broadcaster.
type ActorContext interface {
	// Sends message to any other node.
	Send(payload proto.Message, receiver string) error

	Log() *zap.SugaredLogger

	// Timer function.
	AfterFunc(duration int, f TimerCallback)

	// Deterministic prg.
	RandomInt(n int) int

	// Persistent storage.

	// Writes len(p) bytes from p starting from given offset.
	// Returns number of written bytes and an error, if happened.
	WriteAt(p []byte, off int64) (n int, err error)

	// Reads up to len(p) bytes from storage into p, starting from given offset.
	// Returns number of read bytes and an error, if happened.
	ReadAt(p []byte, off int64) (n int, err error)

	// Current amount of written bytes.
	GetStorageLength() int
}

type BroadcasterCallbacks interface {
	// Is called by the broadcaster, when the entry is reliably stored and ordered.
	Deliver(broadcastValue int64)

	// Is called by the broadcaster, when it wins elections.
	OnStartLeading(term int64)
}

type AtomicBroadcasterCtor func(nodeId, nodeCount int, broadcasterCallbacks BroadcasterCallbacks, context ActorContext) Actor
