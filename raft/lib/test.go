package lib

import (
	"gitlab.com/slon/shad-ds/raft/interfaces"
	"google.golang.org/protobuf/proto"
	"time"
)

const MaxBroadcastsPerTest = 2000
const MaxEventsPerTest = 1000
const MaxSegmentsPerTest = 32
const MaxNodeCount = 32

const WarmupRounds = 100
const TeardownRounds = 1000

// Minimum value of delay used in AfterFunc.
const MinTimerDelay = 10
const StableSegmentError = 0.1

const TestTimeout = 5 * time.Second
const TestSuitTimeout = 20 * time.Second

type BroadcastRequest struct {
	// Timestamp is relative to the begin of the sub-segment broadcast time.
	Timestamp      int
	BroadcastValue int64
	Source         string
}

type SentMessage struct {
	Sender           string
	Receiver         string
	Payload          []byte
	PayloadThunk     proto.Message
	SendTimestamp    int
	DeliverTimestamp int

	TimerCallback *interfaces.TimerCallback
}

type NodeEvent struct {
	// Timestamp is relative to the begin of the unstable sub-segment broadcast time.
	Timestamp int
	Node      string
}

type DelayFunc func(message SentMessage) int

type TestSegment struct {
	// Timestamps for stable broadcast are counted from the moment of warmup completion.
	StableBroadcasts []BroadcastRequest
	StableDelayFunc  DelayFunc

	// Timestamps for best effort broadcasts are counted from the moment of delivery of the last stable event in the TestSegment.
	UnstableBroadcasts []BroadcastRequest
	UnstableDelayFunc  DelayFunc

	// Timestamps for node events are counted from the moment start of unstable subsegment.
	DeathEvents  []NodeEvent
	ReviveEvents []NodeEvent
}

type TestDescriptor struct {
	TestName  string
	NodeCount int

	Segments []TestSegment
}
