package raft

import (
	"gitlab.com/slon/shad-ds/raft/lib"
	"gitlab.com/slon/shad-ds/raft/solution"
	"gitlab.com/slon/shad-ds/raft/tests"
	"testing"
)

func TestExampleIsPassingPublicTests(t *testing.T) {
	lib.RunTests(t, solution.CreateBroadcaster, tests.PublicTests)
}
