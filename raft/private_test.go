// +build private

package raft

import (
	"gitlab.com/slon/shad-ds/private/raft/tests"
	"gitlab.com/slon/shad-ds/raft/lib"
	"gitlab.com/slon/shad-ds/raft/solution"
	"testing"
)

func TestSolutionIsPassingPrivateTests(t *testing.T) {
	lib.RunTests(t, solution.CreateBroadcaster, tests.Tests)
}
