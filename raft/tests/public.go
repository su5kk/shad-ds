package tests

import (
	"gitlab.com/slon/shad-ds/raft/interfaces"
	"gitlab.com/slon/shad-ds/raft/lib"
)

func NoDelayFunc(msg lib.SentMessage) int {
	return msg.DeliverTimestamp
}

func InfDelayFunc(msg lib.SentMessage) int {
	return msg.DeliverTimestamp + 10000*interfaces.AvgDeliveryTime
}

var PublicTests = []lib.TestDescriptor{
	{
		TestName:  "public 1",
		NodeCount: 3,
		Segments: []lib.TestSegment{
			{
				StableBroadcasts: []lib.BroadcastRequest{
					{
						Timestamp:      1000,
						BroadcastValue: 1,
						Source:         interfaces.GetActorId(0),
					},
					{
						Timestamp:      2000,
						BroadcastValue: 2,
						Source:         interfaces.GetActorId(1),
					},
					{
						Timestamp:      3000,
						BroadcastValue: 3,
						Source:         interfaces.GetActorId(2),
					},
				},
				StableDelayFunc: NoDelayFunc,
				UnstableBroadcasts: []lib.BroadcastRequest{
					{
						Timestamp:      1000,
						BroadcastValue: 4,
						Source:         interfaces.GetActorId(0),
					},
					{
						Timestamp:      2000,
						BroadcastValue: 5,
						Source:         interfaces.GetActorId(1),
					},
					{
						Timestamp:      3000,
						BroadcastValue: 6,
						Source:         interfaces.GetActorId(2),
					},
				},
				UnstableDelayFunc: InfDelayFunc,
				DeathEvents:       []lib.NodeEvent{},
				ReviveEvents:      []lib.NodeEvent{},
			},
		},
	},
	{
		TestName:  "public 2",
		NodeCount: 3,
		Segments: []lib.TestSegment{
			{
				StableBroadcasts: []lib.BroadcastRequest{
					{
						Timestamp:      1000,
						BroadcastValue: 1,
						Source:         interfaces.GetActorId(0),
					},
					{
						Timestamp:      2000,
						BroadcastValue: 2,
						Source:         interfaces.GetActorId(0),
					},
				},
				StableDelayFunc: NoDelayFunc,
				UnstableBroadcasts: []lib.BroadcastRequest{
					{
						Timestamp:      16000,
						BroadcastValue: 3,
						Source:         interfaces.GetActorId(1),
					},
				},
				UnstableDelayFunc: NoDelayFunc,
				DeathEvents: []lib.NodeEvent{
					{
						Timestamp: 1000,
						Node:      interfaces.GetActorId(0),
					},
					{
						Timestamp: 2000,
						Node:      interfaces.GetActorId(1),
					},
				},
				ReviveEvents: []lib.NodeEvent{
					{
						Timestamp: 15000,
						Node:      interfaces.GetActorId(1),
					},
				},
			},
			{
				StableBroadcasts: []lib.BroadcastRequest{
					{
						Timestamp:      1000,
						BroadcastValue: 5,
						Source:         interfaces.GetActorId(1),
					},
				},
				StableDelayFunc:    NoDelayFunc,
				UnstableBroadcasts: []lib.BroadcastRequest{},
				UnstableDelayFunc:  InfDelayFunc,
				DeathEvents:        []lib.NodeEvent{},
				ReviveEvents:       []lib.NodeEvent{},
			},
		},
	},
}
