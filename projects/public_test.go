package projects

import (
	"testing"

	"gitlab.com/slon/shad-ds/projects/lib"
	"gitlab.com/slon/shad-ds/projects/solution"
	"gitlab.com/slon/shad-ds/projects/tests"
)

func TestPublic(t *testing.T) {
	lib.RunTests(t, solution.NewUsersService, solution.NewProjectsService, tests.PublicTests)
}
