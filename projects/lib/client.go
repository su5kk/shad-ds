package lib

import (
	"fmt"

	"gitlab.com/slon/shad-ds/projects/interfaces"
	"go.uber.org/atomic"
	"go.uber.org/zap"
)

var MonotonicRequestId atomic.Int64
var MonotonicClientId atomic.Int64

type ShutdownError struct {
}

func (e ShutdownError) Error() string {
	return "simulation finished"
}

func generateId() string {
	return fmt.Sprintf("request_%v", MonotonicRequestId.Inc())
}

func validateSender(expectedSender string, message interfaces.Message) {
	if expectedSender != message.Sender {
		panic(fmt.Errorf("expected sender %q, but got %q", expectedSender, message.Sender))
	}
}

type GetResponse struct {
	Err    error
	Values []string
}

type CreateResponseChannel chan error
type GetResponseChannel chan GetResponse

type Client struct {
	Id                   string
	OutstandingRequestId *string
	OutstandingCreate    CreateResponseChannel
	OutstandingGet       GetResponseChannel
	ActorSystem          *ActorSystem
	ActorContext         *ActorContext

	Finished bool
}

func (c *Client) Log() *zap.SugaredLogger {
	return c.ActorContext.Log()
}

func (c *Client) Shutdown() {
	c.Finished = true

	if c.OutstandingCreate != nil {
		responseChannel := c.OutstandingCreate
		c.OutstandingCreate = nil
		c.OutstandingRequestId = nil
		responseChannel <- ShutdownError{}
	}

	if c.OutstandingGet != nil {
		responseChannel := c.OutstandingGet
		c.OutstandingGet = nil
		c.OutstandingRequestId = nil
		getResponse := GetResponse{
			Err:    ShutdownError{},
			Values: nil,
		}
		responseChannel <- getResponse
	}
}

func (c *Client) Receive(message interfaces.Message) {
	switch m := message.Payload.(type) {
	case *interfaces.CreateUserRsp:
		validateSender(interfaces.UsersActorId, message)
		c.processCreateUserResponse(m)

	case *interfaces.CreateProjectRsp:
		validateSender(interfaces.ProjectsActorId, message)
		c.processCreateProjectResponse(m)

	case *interfaces.GetUserProjectsRsp:
		validateSender(interfaces.UsersActorId, message)
		c.processGetUserProjectsResponse(m)

	case *interfaces.GetProjectUsersRsp:
		validateSender(interfaces.ProjectsActorId, message)
		c.processGetProjectUsersResponse(m)

	default:
		panic(fmt.Errorf("client received unexpected message type %T", message))
	}
}

func (c *Client) processCreateUserResponse(message *interfaces.CreateUserRsp) {
	c.validateResponseId(message.RequestId)
	if c.OutstandingCreate == nil {
		panic("received create user response, but response channel does not exist")
	}
	responseChannel := c.OutstandingCreate
	c.OutstandingCreate = nil
	if message.Error.Ok {
		responseChannel <- nil
	} else {
		responseChannel <- fmt.Errorf(message.Error.Message)
	}
}

func (c *Client) processGetUserProjectsResponse(message *interfaces.GetUserProjectsRsp) {
	c.validateResponseId(message.RequestId)
	if c.OutstandingGet == nil {
		panic("received get user projects response, but response channel does not exist")
	}
	responseChannel := c.OutstandingGet
	c.OutstandingGet = nil
	if message.Error.Ok {
		getResponse := GetResponse{
			Err:    nil,
			Values: message.Projects,
		}
		responseChannel <- getResponse
	} else {
		getResponse := GetResponse{
			Err:    fmt.Errorf(message.Error.Message),
			Values: message.Projects,
		}
		responseChannel <- getResponse
	}
}

func (c *Client) processCreateProjectResponse(message *interfaces.CreateProjectRsp) {
	c.validateResponseId(message.RequestId)
	if c.OutstandingCreate == nil {
		panic("received create project response, but response channel does not exist")
	}
	responseChannel := c.OutstandingCreate
	c.OutstandingCreate = nil
	if message.Error.Ok {
		responseChannel <- nil
	} else {
		responseChannel <- fmt.Errorf(message.Error.Message)
	}
}

func (c *Client) processGetProjectUsersResponse(message *interfaces.GetProjectUsersRsp) {
	c.validateResponseId(message.RequestId)
	if c.OutstandingGet == nil {
		panic("received get user projects response, but response channel does not exist")
	}
	responseChannel := c.OutstandingGet
	c.OutstandingGet = nil
	if message.Error.Ok {
		getResponse := GetResponse{
			Err:    nil,
			Values: message.Users,
		}
		responseChannel <- getResponse
	} else {
		getResponse := GetResponse{
			Err:    fmt.Errorf(message.Error.Message),
			Values: message.Users,
		}
		responseChannel <- getResponse
	}
}

func (c *Client) validateConcurrentRequests() {
	if c.OutstandingRequestId != nil {
		panic("client cannot serve concurrent requests")
	}
}

func (c *Client) validateResponseId(requestId string) {
	if c.OutstandingRequestId == nil {
		panic("client was not expecting response")
	}

	if *c.OutstandingRequestId != requestId {
		panic(fmt.Errorf("received response with unexpected id %q instead of %q", requestId, *c.OutstandingRequestId))
	}

	c.OutstandingRequestId = nil
}

func (c *Client) CreateUser(user string, projects []string) CreateResponseChannel {
	requestId := generateId()
	responseChan := make(CreateResponseChannel, 1)

	var req interfaces.CreateUserReq
	req.RequestId = requestId
	req.User = user
	req.Projects = projects

	callback := func() {
		c.validateConcurrentRequests()

		c.OutstandingRequestId = &requestId
		c.OutstandingCreate = responseChan

		if c.Finished {
			c.Shutdown()
		} else {
			err := c.ActorContext.Send(&req, interfaces.UsersActorId)
			if err != nil {
				panic(fmt.Errorf("failed to send message to \"users\" actor, %v", err))
			}
		}
	}
	c.ActorSystem.clientCallbacks <- callback
	return responseChan
}

func (c *Client) CreateProject(project string, users []string) CreateResponseChannel {
	requestId := generateId()
	responseChan := make(CreateResponseChannel, 1)

	var req interfaces.CreateProjectReq
	req.RequestId = requestId
	req.Project = project
	req.Users = users

	callback := func() {
		c.validateConcurrentRequests()

		c.OutstandingRequestId = &requestId
		c.OutstandingCreate = responseChan

		if c.Finished {
			c.Shutdown()
		} else {
			err := c.ActorContext.Send(&req, interfaces.ProjectsActorId)
			if err != nil {
				panic(fmt.Errorf("failed to send message to \"projects\" actor, %v", err))
			}
		}
	}
	c.ActorSystem.clientCallbacks <- callback
	return responseChan
}

func (c *Client) GetUserProjects(user string, consistent bool) GetResponseChannel {
	requestId := generateId()
	responseChan := make(GetResponseChannel, 1)

	var req interfaces.GetUserProjectsReq
	req.RequestId = requestId
	req.User = user
	req.Consistent = consistent

	callback := func() {
		c.validateConcurrentRequests()

		c.OutstandingRequestId = &requestId
		c.OutstandingGet = responseChan

		if c.Finished {
			c.Shutdown()
		} else {
			err := c.ActorContext.Send(&req, interfaces.UsersActorId)
			if err != nil {
				panic(fmt.Errorf("failed to send message to \"users\" actor, %v", err))
			}
		}
	}
	c.ActorSystem.clientCallbacks <- callback
	return responseChan
}

func (c *Client) GetProjectUsers(project string, consistent bool) GetResponseChannel {
	requestId := generateId()
	responseChan := make(GetResponseChannel, 1)

	var req interfaces.GetProjectUsersReq
	req.RequestId = requestId
	req.Project = project
	req.Consistent = consistent

	callback := func() {
		c.validateConcurrentRequests()

		c.OutstandingRequestId = &requestId
		c.OutstandingGet = responseChan

		if c.Finished {
			c.Shutdown()
		} else {
			err := c.ActorContext.Send(&req, interfaces.ProjectsActorId)
			if err != nil {
				panic(fmt.Errorf("failed to send message to \"projects\" actor, %v", err))
			}
		}
	}

	c.ActorSystem.clientCallbacks <- callback

	return responseChan
}

func NewClient(actorSystem *ActorSystem) *Client {
	clientId := fmt.Sprintf("client_%v", MonotonicClientId.Inc())
	context := ActorContext{
		actorSystem: actorSystem,
		id:          clientId,
	}
	client := Client{
		Id:           clientId,
		ActorContext: &context,
		ActorSystem:  actorSystem,
		Finished:     false,
	}
	err := actorSystem.registerClient(clientId, &client)
	if err != nil {
		panic(fmt.Errorf("failed to register new client, %v", err))
	}
	return &client
}
